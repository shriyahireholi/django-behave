from splinter.browser import Browser

def before_all(context):
    browser = Browser()
    context.browser = browser

def after_all(context):
    context.browser.quit()
    context.browser = None