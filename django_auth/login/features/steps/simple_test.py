from behave import *
import time

@given(u'User enters "{username}" as username and "{password}" as password and submits')
def try_login(context, username, password):
    context.browser.visit("http://127.0.0.1:8000/admin/")
    context.browser.find_by_name('username').fill(username)
    context.browser.find_by_name('password').fill(password)
    context.browser.find_by_xpath('/html/body/div/div[2]/div/div[1]/div/form/div[3]/input').first.click()


@given(u'User clicks logout link')
def try_logout(context):
    context.browser.find_by_xpath("/html/body/div/div[1]/div[2]/a[3]").first.click()


@then(u'Django Administration page can be accessed')
def check_message(context):
    pass

@then(u'"{msg}" should be displayed')
def check_message(context, msg):
    context.browser.find_by_xpath('/html/body/div/div[3]/div/div[1]/h1').text


@then(u'"{msg}" credential error is displayed')
def check_message(context, msg):
    context.browser.find_by_xpath('/html/body/div/div[2]/div/div[1]/p').text
