Feature: User can login and use authentication

	# superuser successful login 
    Scenario: Successful Login 
		Given User enters "shriya" as username and "root" as password and submits
		Then Django Administration page can be accessed

	Scenario: Successfull Logout
		Given User clicks logout link
		Then "Logged out" should be displayed

	#superuser unsucessful login
	Scenario: Unsuccessful Login
		Given User enters "shriyaa" as username and "test" as password and submits
		Then "Please enter the correct username and password for a staff account. Note that both fields may be case-sensitive." credential error is displayed

	#staff user cannot access Django admin page, therefore unsucessful login
	Scenario: Unsuccessful Login
		Given User enters "test" as username and "test" as password and submits
		Then "Please enter the correct username and password for a staff account. Note that both fields may be case-sensitive." credential error is displayed