from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
from behave import *
import time


@given(u'User enters "{username}" as username and "{password}" as password and submits')
def try_login(context, username, password):
    context.browser.get(context.root_url + '/admin')
    try:
        uname_elem = context.browser.find_element_by_name("username")
        uname_elem.send_keys(username)
        passwd_elem = context.browser.find_element_by_name("password")
        passwd_elem.send_keys(password)
        submit_elem = context.browser.find_element_by_xpath("/html/body/div/div[2]/div/div[1]/div/form/div[3]/input").click()
    except NoSuchElementException:
        assert False, 'Element not Found'


@given(u'User clicks logout link')
def try_logout(context):
    try:
        logout_link = context.browser.find_element_by_xpath("/html/body/div/div[1]/div[2]/a[3]").click()
    except NoSuchElementException:
	    assert False, 'Element not Found'


@then(u'Django Administration page can be accessed')
def step_impl(context):
    pass

@then(u'"{msg}" should be displayed')
def check_message(context, msg):
    try:
        msg_elem = context.browser.find_element_by_xpath('/html/body/div/div[3]/div/div[1]/h1').text
        assert msg
    except NoSuchElementException:
        assert False, 'Element not Found'


@then(u'"{msg}" credential error is displayed')
def check_message(context, msg):
    try:
        msg = context.browser.find_element_by_class_name('errornote').text
        assert msg
    except NoSuchElementException:
        assert False, 'Element not Found'