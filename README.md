# Automation testing in Django

## To run this project

Clone this repo

Move to the folder  django-behave

Create a virtual environment and activate it.
```bash
$ virtualenv venv
$ source venv/bin/activate
```

Install all dependencies

```bash
$ pip3 install -r requirements.txt
```

<hr>

### To setup databse and create superuser and staff user

```bash 
$ cd django-auth
$ python3 manage.py migrate
$ python3 manage.py createsuperuser
$ python3 manage.py shell
>>> from django.contrib.auth.models import User
>>> user = User.objects.create_user(username='test', password='test')
>>> user.save()
```

Later replace the user credentials in *featre file*

<hr>

### Behave selenium testing

```bash  
$ cd django-auth
$ python3 manage.py runserver
$ behave
```

<hr>

### Behave Django test-runner

```bash
$ cd django-auth
$ python3 manage.py runserver
$ python3 manage.py test
```